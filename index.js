class QuantrSP {
	constructor(username, password, domain) {
		this.username = username;
		this.password = password;
		this.domain = domain;
	}

	version() {
		var pjson = require('./package.json');
		return pjson.version;
	}

	login() {
		var that = this;
		return new Promise(function (resolve, reject) {
			var request = that.requestToken();
			request.then(function (xml) {
				var format = require('xml-formatter');
				//console.log(format(xml));
				var xpath = require('xpath');
				var dom = require('xmldom').DOMParser;
				var doc = new dom().parseFromString(xml);
				var select = xpath.useNamespaces({
					"wsse": "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"
				});
				try{
					var nodes = select("//wsse:BinarySecurityToken", doc);

					var token = nodes[0].firstChild.data;
					var request2 = that.submitToken(token);
					request2.then(function (token) {
						resolve(token);
					});
				}catch(ex){
					resolve("Error: Login incorrect");
				}
			});
		});
	}

	requestToken() {
		var https = require('https');
		var saml = this.generateSAML();
		const data = saml;
		const options = {
			hostname: 'login.microsoftonline.com',
			port: 443,
			path: '/extSTS.srf',
			method: 'POST',
			headers: {
				'Content-Type': 'text/xml; charset=utf-8',
				'Content-Length': data.length
			}
		};
		//console.log(data);

		return new Promise(function (resolve, reject) {
			const req = https.request(options, (res) => {
				// console.log(`statusCode: ${res.statusCode}`);
				var data = "";

				res.on('data', (d) => {
					data += d.toString();
				});
				res.on("end", function () {
					resolve(data);
				});
			});

			req.on('error', (error) => {
				console.error(error);
			});
			req.write(data);
			req.end();
		});
	}

	generateSAML() {
		var saml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
			"<s:Envelope xmlns:s=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:a=\"http://www.w3.org/2005/08/addressing\" xmlns:u=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">\n" +
			"   <s:Header>\n" +
			"      <a:Action s:mustUnderstand=\"1\">http://schemas.xmlsoap.org/ws/2005/02/trust/RST/Issue</a:Action>\n" +
			"      <a:ReplyTo>\n" +
			"         <a:Address>http://www.w3.org/2005/08/addressing/anonymous</a:Address>\n" +
			"      </a:ReplyTo>\n" +
			"      <a:To s:mustUnderstand=\"1\">https://login.microsoftonline.com/extSTS.srf</a:To>\n" +
			"      <o:Security xmlns:o=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" s:mustUnderstand=\"1\">\n" +
			"         <o:UsernameToken>\n" +
			"            <o:Username>" + this.username + "</o:Username>\n" +
			"            <o:Password>" + this.password + "</o:Password>\n" +
			"         </o:UsernameToken>\n" +
			"      </o:Security>\n" +
			"   </s:Header>\n" +
			"   <s:Body>\n" +
			"      <t:RequestSecurityToken xmlns:t=\"http://schemas.xmlsoap.org/ws/2005/02/trust\">\n" +
			"         <wsp:AppliesTo xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\">\n" +
			"            <a:EndpointReference>\n" +
			"               <a:Address>https://" + this.domain + "/_forms/default.aspx?wa=wsignin1.0</a:Address>\n" +
			"            </a:EndpointReference>\n" +
			"         </wsp:AppliesTo>\n" +
			"         <t:KeyType>http://schemas.xmlsoap.org/ws/2005/05/identity/NoProofKey</t:KeyType>\n" +
			"         <t:RequestType>http://schemas.xmlsoap.org/ws/2005/02/trust/Issue</t:RequestType>\n" +
			"         <t:TokenType>urn:oasis:names:tc:SAML:1.0:assertion</t:TokenType>\n" +
			"      </t:RequestSecurityToken>\n" +
			"   </s:Body>\n" +
			"</s:Envelope>";
		return saml;
	}


	submitToken(token) {
		// console.log(token);
		var https = require('https');
		const data = token;
		// console.log(this.domain + '/_forms/default.aspx?wa=wsignin1.0');
		const options = {
			hostname: this.domain,
			port: 443,
			path: '/_forms/default.aspx?wa=wsignin1.0',
			method: 'POST',
			headers: {
				// 'Content-Type': 'text/xml; charset=utf-8',
				// 'Content-Length': data.length,
				'Accept': "application/x-www-form-urlencoded"
			}
		};

		return new Promise(function (resolve, reject) {
			const req = https.request(options, (res) => {
				// console.log(`statusCode: ${res.statusCode}`);
				//console.log(`headers: ${JSON.stringify(res.headers['set-cookie'][0])}`);

				var cookies = res.headers['set-cookie'];
				var rtFa;
				var FedAuth;
				for (var x = 0; x < cookies.length; x++) {
					// console.log(cookies[x]);
					if (cookies[x].toString().indexOf('rtFa') != -1) {
						rtFa = cookies[x];
					} else if (cookies[x].toString().indexOf('FedAuth') != -1) {
						FedAuth = cookies[x];
					}
				}
				resolve({
					rtFa,
					FedAuth
				});
			});

			req.on('error', (error) => {
				console.error(error);
			});
			req.write(data);
			req.end();
		});
	}

	get(token, path) {
		var https = require('https');
		const options = {
			hostname: this.domain,
			port: 443,
			path: path,
			method: 'GET',
			headers: {
				'Cookie': token.rtFa + ";" + token.FedAuth,
				'Accept': "application/json;odata=verbose"
			}
		};

		return new Promise(function (resolve, reject) {
			const req = https.request(options, (res) => {
				// console.log(`statusCode: ${res.statusCode}`);
				var data = "";

				res.on('data', (d) => {
					data += d.toString();
				});
				res.on("end", function () {
					resolve(JSON.parse(data));
				});
			});

			req.on('error', (error) => {
				console.error(error);
			});
			req.end();
		});
	}

	post(token, path, json, formDigestValue, isMerge) {
		var https = require('https');
		const options = {
			hostname: this.domain,
			port: 443,
			path: path,
			method: 'POST',
			headers: {
				'Cookie': token.rtFa + ";" + token.FedAuth,
				'Accept': "application/json;odata=verbose",
				"content-type": "application/json;odata=verbose",
				"X-RequestDigest": formDigestValue,
				"IF-MATCH": "*"
			}
		};

		if (isMerge) {
			options.headers['X-HTTP-Method'] = 'MERGE';
		}

		return new Promise(function (resolve, reject) {
			const req = https.request(options, (res) => {
				console.log(`statusCode: ${res.statusCode}`);
				var data = "";

				res.on('data', (d) => {
					data += d.toString();
				});
				res.on("end", function () {
					resolve(JSON.parse(data));
				});
			});

			req.on('error', (error) => {
				console.error(error);
			});
			if (json != null) {
				console.log(JSON.stringify(json));
				req.write(JSON.stringify(json));
			}
			req.end();
		});
	}
}

module.exports = QuantrSP;