# Quantr JS Library

Calling SharePoint in pure Javascript, no need typescript, best to use in ElectronJS

## publish to npmjs

```
npm publish --access public
```

## example

```
var QuantrSP = require('@quantr/quantr-js-library');

var domain = "quantr.sharepoint.com";
var site = "test";
var docLib = "doclib1";
var username = "peter@quantr.hk";
var password = "password";

var quantrSP = new QuantrSP(username, password, domain);
quantrSP.login().then(function (token) {
	quantrSP.get(token, '/_api/web/webs').then(function (json) {
		console.log(json.d.results[0]);
	});

	quantrSP.post(token, '/_api/contextinfo', null, null, false).then(function (json) {
		var formDigestValue = json.d.GetContextWebInformation.FormDigestValue;
		console.log(formDigestValue);

		var json = {
			'parameters': {
				'__metadata': {
					'type': 'SP.WebCreationInformation'
				},
				'Title': 'Social Meetup',
				'Url': 'social',
				'WebTemplate': 'MPS#3',
				'UseSamePermissionsAsParentSite': true
			}
		};
		quantrSP.post(token, '/_api/web/webs/add', json, formDigestValue, false).then(function (json) {
			console.log(json);
		});
	});

});
```
