const QuantrSP = require('./index.js');
//const QuantrSP = require('@quantr/quantr-js-library');


let domain = "quantr.sharepoint.com";
let site = "test";
let docLib = "doclib1";
let username = "peter@quantr.hk";
let password = "password";

const quantrSP = new QuantrSP(username, password, domain);

console.log('start');
quantrSP.login().then((token) => {
	console.log(token.rtFa);
	console.log(token.FedAuth);

    // get all webs
    // quantrSP.get(token, '/_api/web/webs').then((json) => {
    //     console.log("Test: get all Webs (GET)")
    //     if (json !== null || json !== undefined) {
    //         console.log(json.d.results[0]);
    //     }
    //     console.log('-----------------------------------');
	// });
	

    // get all site collections
    quantrSP.get(token, "/_api/search/query?querytext='contentclass:sts_site'").then((json) => {
		console.log("Test: get all site collections (GET)");
		console.log(json);
        if (json !== null || json !== undefined) {
            console.log(json.d.results[0]);
        }
        console.log('-----------------------------------');
    })

    // // get all sites
    // quantrSP.get(token, "/_api/site").then((json) => {
    //     console.log("Test: get all sites (GET)")
    //     if (json !== null || json !== undefined) {
    //         console.log(json.d.results[0]);
    //     }
    //     console.log('-----------------------------------');
    // })

    // // add a site
    // quantrSP.post(token, '/_api/contextinfo', null, null, false).then((json) => {
    //     console.log("Test: add a site (POST)")
    //     let formDigestValue = json.d.GetContextWebInformation.FormDigestValue;
    //     console.log(formDigestValue);

    //     let data = {
    //         'parameters': {
    //             '__metadata': {
    //                 'type': 'SP.WebCreationInformation'
    //             },
    //             'Title': 'Social Meetup',
    //             'Url': 'social',
    //             'WebTemplate': 'MPS#3',
    //             'UseSamePermissionsAsParentSite': true
    //         }
    //     };

    //     quantrSP.post(token, '/_api/web/webs/add', data, formDigestValue, false).then((json) => {
    //         console.log("Test: add a site (POST)")
    //         if (json !== null || json !== undefined) {
    //             console.log(json);
    //         }
    //         console.log('-----------------------------------');
    //     });
    // });

    // // change site description
    // quantrSP.post(token, '/_api/contextinfo', null, null, false).then((json) => {
    //     console.log("Test: change site description (POST)")
    //     let formDigestValue = json.d.GetContextWebInformation.FormDigestValue;
    //     let data = {
    //         '__metadata': {
    //             'type': 'SP.Web'
    //         },
    //         'Description': 'my testing description',
    //         'EnableMinimalDownload': false
    //     };

    //     quantrSP.post(token, '/social/_api/web', data, formDigestValue, true).then((json) => {
    //         if (json !== null || json !== undefined) {
    //             console.log(json);
    //         }
    //         console.log('-----------------------------------');
    //     });
    // });

    // // delete a site

    // // get all lists
    // quantrSP.get(token, "/_api/web/lists?" + encodeURI("$select=ID,Title&$filter=basetype ne 1&$orderby=title")).then((json) => {
    //     console.log("Test: get all lists (GET)")
    //     if (json !== null || json !== undefined) {
    //         console.log(json.d.results[0]);
    //     }
    //     console.log('-----------------------------------');
    // })

    // // get all lists with tile and guid only
    // quantrSP.get(token, "/_api/web/lists?$select=ID,Title").then((json) => {
    //     console.log("Test: get all lists with tile and guid only (GET)")
    //     if (json !== null || json !== undefined) {
    //         console.log(json.d.results[0]);
    //     }

    //     console.log('-----------------------------------');
    // })

    // // get list by specific ID
    // quantrSP.get(token, "/_api/web/lists(guid'8f0cd839-88c1-4fea-ae05-19f7df1f2645')").then((json) => {
    //     console.log("Test: get list by specific ID (GET)")
    //     if (json !== null || json !== undefined) {
    //         console.log(json.d.results[0]);
    //     }

    //     console.log('-----------------------------------');
    // })

    // // get list by specific title
    // quantrSP.get(token, "/_api/web/lists/GetByTitle('Workflow%20Tasks')").then((json) => {
    //     console.log("Test: get list by specific title (GET)")
    //     if (json !== null || json !== undefined) {
    //         console.log(json.d.results[0]);
    //     }

    //     console.log('-----------------------------------');
    // })

    // // create a list called Peter
    // quantrSP.post(token, '/_api/contextinfo', null, null, false).then((json) => {
    //     console.log("Test: create a list called Peter (POST)")
    //     let formDigestValue = json.d.GetContextWebInformation.FormDigestValue;
    //     let data = {
    //         '__metadata': {
    //             'type': 'SP.List'
    //         },
    //         'AllowContentTypes': true,
    //         'BaseTemplate': 100,
    //         'ContentTypesEnabled': true,
    //         'Description': 'created by SharePoint-Java-API',
    //         'Title': 'Peter'
    //     };

    //     quantrSP.post(token, "/_api/web/lists", data, formDigestValue, false).then((json) => {
    //         if (json !== null || json !== undefined) {
    //             console.log(json);
    //         }
    //         console.log('-----------------------------------');
    //     });
    // });

    // // change list name from Peter to John
    // quantrSP.post(token, '/_api/contextinfo', null, null, false).then((json) => {
    //     let formDigestValue = json.d.GetContextWebInformation.FormDigestValue;
    //     let data = {
    //         '__metadata': {
    //             'type': 'SP.List'
    //         },
    //         'AllowContentTypes': true,
    //         'BaseTemplate': 100,
    //         'ContentTypesEnabled': true,
    //         'Description': 'new description',
    //         'Title': 'John'
    //     };

    //     quantrSP.post(token, "/_api/web/lists/GetByTitle('Peter')", data, formDigestValue, false).then((json) => {
    //         console.log("Test: change list name from Peter to John (POST)")
    //         if (json !== null || json !== undefined) {
    //             console.log(json);
    //         }
    //         console.log('-----------------------------------');
    //     });
    // });

    // // add column to list John, for FieldTypeKind references to https://msdn.microsoft.com/en-us/library/microsoft.sharepoint.client.fieldtype.aspx
    // quantrSP.post(token, '/_api/contextinfo', null, null, false).then((json) => {
    //     let formDigestValue = json.d.GetContextWebInformation.FormDigestValue;
    //     let data = {
    //         '__metadata': {
    //             'type': 'SP.Field'
    //         },
    //         'FieldTypeKind': 11,
    //         'Title': 'my new column'
    //     };

    //     quantrSP.post(token, "/_api/web/lists/GetByTitle('John')/Fields", data, formDigestValue, false).then((json) => {
    //         console.log("Test: add column to list John (POST)")
    //         if (json !== null || json !== undefined) {
    //             console.log(json);
    //         }
    //         console.log('-----------------------------------');
    //     });
    // });

    // // insert an item to list John, the list was called Peter by creation, so it is SP.Data.PeterListItem, not SP.Data.JohnListItem
    // quantrSP.post(token, '/_api/contextinfo', null, null, false).then((json) => {
    //     console.log("Test: insert an item to list John (POST)")
    //     let formDigestValue = json.d.GetContextWebInformation.FormDigestValue;
    //     let data = {
    //         '__metadata': {
    //             'type': 'SP.Data.PeterListItem'
    //         },
    //         'Title': 'test1',
    //         'my_x0020_new_x0020_column': {
    //             'Url': 'http://www.google.com',
    //             'Description': 'Google USA'
    //         }
    //     };

    //     quantrSP.post(token, "/_api/web/lists/GetByTitle('John')/items", data, formDigestValue, false).then((json) => {
    //         if (json !== null || json !== undefined) {
    //             console.log(json);
    //         }
    //         console.log('-----------------------------------');
    //     });
    // });

    // // get list items from list John
    // quantrSP.get(token, "/_api/web/lists/GetByTitle('John')/items").then((json) => {
    //     console.log("Test: get list items from list John (GET)")
    //     if (json !== null || json !== undefined) {
    //         console.log(json.d.results[0]);
    //     }
    //     console.log('-----------------------------------');
    // });

    // // delete list john

    // // use caml retrieve list item
    // let data = {
    //     "query": {
    //         "__metadata": {
    //             "type": "SP.CamlQuery",
    //             "ViewXml": "<View>"
    //                 + "<RowLimit>4</RowLimit>"
    //                 + "<Query>\n"
    //                 + "   <Where>\n"
    //                 + "      <Contains>\n"
    //                 + "         <FieldRef Name='FileLeafRef' />\n"
    //                 + "         <Value Type='Text'>xlsx</Value>\n"
    //                 + "      </Contains>\n"
    //                 + "   </Where>\n"
    //                 + "   <OrderBy>\n"
    //                 + "      <FieldRef Name='FileLeafRef' Ascending='False' />\n"
    //                 + "   </OrderBy>\n"
    //                 + "</Query>"
    //                 + "</View>"
    //         }
    //     }
    // }
    // quantrSP.post(token, '/_api/contextinfo', null, null, false).then((json) => {
    //     console.log("Test: use caml retrieve list item")
    //     let formDigestValue = json.d.GetContextWebInformation.FormDigestValue;
    //     console.log(JSON.stringify(data));

    //     quantrSP.post(token, "/_api/web/lists/GetByTitle('doclib2')/GetItems?$select=FileLeafRef", JSON.stringify(data), formDigestValue, false).then((json) => {
    //         if (json !== null || json !== undefined) {
    //             console.log("length=" + JSON.parse(json).d.results.length);
    //             console.log(json)
    //         }
    //         console.log('-----------------------------------');
    //     });
    // });

    // // retrieve items using default view
    // quantrSP.post(token, '/_api/contextinfo', null, null, false).then((json) => {
    //     console.log("Test: retrieve items using default view")
    //     let formDigestValue = json.d.GetContextWebInformation.FormDigestValue;
    //     quantrSP.post(token, "/_api/web/lists/GetByTitle('doclib2')/DefaultView", null, formDigestValue, false).then((jsonString) => {
    //         if (jsonString !== null || jsonString !== undefined) {
    //             let json = JSON.parse(jsonString);
    //             let viewQuery = toString(json.d.ViewQuery);
    //             console.log("viewQuery=" + viewQuery);
    //             delete data["__metadata"]["ViewXml"];
    //             data["__metadata"]["ViewXml"] = "<View>"
    //                 + "<Query>" + viewQuery + "</Query>"
    //                 + "</View>";

    //             quantrSP.post(token, "/_api/web/lists/GetByTitle('doclib2')/GetItems?$select=FileLeafRef", JSON.stringify(data), formDigestValue, false).then((res) => {
    //                 if (res !== null || res !== undefined) {
    //                     let resJson = JSON.parse(res);
    //                     console.log("length=" + JSON.parse(resJson).d.results.length);
    //                     console.log(jsonString);
    //                 }
    //             });
    //         }
    //         console.log('-----------------------------------');
    //     });
    // });
}, (error) => {
    console.log("Login Failed")
});
